#Angular.Admin

基于 RequireJs+AngluarJs+Bootstap 的管理平台

### 界面截图
![image](http://angular.html.ctolog.com/screen/1.png)
![image](http://angular.html.ctolog.com/screen/2.png)

###最终目标
完善整套REST方案

###技术工具

服务端：PHP CodeIgnite3 + MySQL

客户端：RequireJs + AngluarJs + Boostrap + jQuery

###阶段里程碑

一. 前端UI构建

二. 后端Oauth权限机制

三. 数据动态化 